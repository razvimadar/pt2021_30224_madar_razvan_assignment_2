package Main;

import java.io.*;

public class FileWriterClass {
    private File outputFile;
    private FileWriter writer;
    private PrintWriter printWriter;

    public FileWriterClass() {
        this.outputFile = new File("out3.txt");
        try {
            this.writer = new FileWriter(this.outputFile);
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.printWriter = new PrintWriter(writer);
    }

    public File getOutputFile() {
        return outputFile;
    }

    public void setOutputFile(File outputFile) {
        this.outputFile = outputFile;
    }

    public FileWriter getWriter() {
        return writer;
    }

    public void setWriter(FileWriter writer) {
        this.writer = writer;
    }

    public PrintWriter getPrintWriter() {
        return printWriter;
    }

    public void setPrintWriter(PrintWriter printWriter) {
        this.printWriter = printWriter;
    }
}
