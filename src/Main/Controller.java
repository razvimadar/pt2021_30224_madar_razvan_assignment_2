package Main;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable  {
    @FXML
    private Button startButton;
    @FXML
    private Button closeButton;
    @FXML
    private ComboBox<String> comboBox = new ComboBox<>();
    SimulationManager simulationManager;
    Thread t1;
    @FXML
    private ImageView image;
    @FXML
    private TextField clientsField;
    @FXML
    private TextField queuesField;
    @FXML
    private TextField minArrival;
    @FXML
    private TextField maxArrival;
    @FXML
    private TextField minService;
    @FXML
    private TextField maxService;
    @FXML
    private TextField maxTime;
    @FXML
    public TextArea textArea = new TextArea();

    public int getTimeLimit() {
        return Integer.parseInt(maxTime.getText());
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        File imageFile = new File("./src/Main/client.png");
        Image image2 = new Image(imageFile.toURI().toString());
        image.setImage(image2);
        comboBox.getItems().addAll(
                "SHORTEST_TIME",
                "SHORTEST_QUEUE"
        );
        startButtonOnAction();
    }

    private void startButtonOnAction() {
        startButton.setOnAction(actionEvent1 -> {
            try {
                int numberOfClients = Integer.parseInt(clientsField.getText());
                int numberOfServers = Integer.parseInt(queuesField.getText());
                int minArrivalTime = Integer.parseInt(minArrival.getText());
                int maxArrivalTime = Integer.parseInt(maxArrival.getText());
                int minProcessingTime = Integer.parseInt(minService.getText());
                int maxProcessingTime = Integer.parseInt(maxService.getText());
                int timeLimit = Integer.parseInt(maxTime.getText());
                String comboBoxValue = comboBox.getValue();
                System.out.println(comboBoxValue);
                if (comboBoxValue.equalsIgnoreCase(String.valueOf(Strategy.SelectionPolicy.SHORTEST_TIME))) {
                    Strategy.SelectionPolicy selectionPolicy = Strategy.SelectionPolicy.SHORTEST_TIME;
                    simulationManager = new SimulationManager(numberOfClients, numberOfServers, minArrivalTime, maxArrivalTime, minProcessingTime, maxProcessingTime, timeLimit, selectionPolicy);
                } else if (comboBoxValue.equalsIgnoreCase(String.valueOf(Strategy.SelectionPolicy.SHORTEST_QUEUE))) {
                    Strategy.SelectionPolicy selectionPolicy = Strategy.SelectionPolicy.SHORTEST_QUEUE;
                    simulationManager = new SimulationManager(numberOfClients, numberOfServers, minArrivalTime, maxArrivalTime, minProcessingTime, maxProcessingTime, timeLimit, selectionPolicy);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            t1 = new Thread(simulationManager);
            t1.setDaemon(true);
            t1.start();
            createThreadUpdateUI(simulationManager);
       });
    }

    public void closeButtonOnAction() {
        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }

    public void createThreadUpdateUI(SimulationManager simulationManager) {
        Thread updateUiThread = new Thread(new Runnable() {
            @Override
            public void run() {
                int currentTime = 0;
                while (currentTime < getTimeLimit() + 1) {
                    textArea.setText(simulationManager.getOutput().toString());
                    if (simulationManager.generatedTasks.size() == 0 && simulationManager.emptyServers(simulationManager.getScheduler()))
                        break;
                    currentTime++;
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                textArea.setText(simulationManager.getOutput().toString());
            }
        });
        updateUiThread.setDaemon(true);
        updateUiThread.start();

    }
}
