package Main;

import java.util.List;

public class ConcreteStrategyQueue implements Strategy{
    @Override
    public void addTask(List<Server> servers, Task t) {
        int min = 99999;
        if (servers.size() > 0)
            min = servers.get(0).getWaitingPeriod().get();
        for (Server si: servers) {
            if (si.getTasks().size() < min)
                min = si.getTasks().size();
        }
        for (Server si: servers) {
            if (si.getTasks().size() == min) {
                si.addTask(t);
                break;
            }
        }
    }
}
