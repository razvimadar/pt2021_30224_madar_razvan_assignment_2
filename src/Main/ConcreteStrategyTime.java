package Main;

import com.sun.javafx.collections.MappingChange;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConcreteStrategyTime implements Strategy {
    Map<Server, Integer> hashMap = new HashMap<>();

    @Override
    public void addTask(List<Server> servers, Task t) {
        int min = 99999;
        if (servers.size() > 0)
            min = servers.get(0).getWaitingPeriod().get();
        for (Server si: servers) {
            if (si.getWaitingPeriod().get() < min)
                min = si.getWaitingPeriod().get();
        }
        for (Server si: servers) {
            if (si.getWaitingPeriod().get() == min) {
                si.addTask(t);
                hashMap.put(si, t.getProcessingTime());
                break;
            }
        }
    }
}
