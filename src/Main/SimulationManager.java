package Main;

import Main.Strategy.*;
import javafx.application.Platform;
import javafx.scene.control.TextArea;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.List;

public class SimulationManager implements Runnable {
    private int timeLimit;
    private int maxProcessingTime;
    private int minProcessingTime;
    private int numberOfServers;
    private int numberOfClients;
    private int maxArrivalTime;
    private int minArrivalTime;
    private Scheduler scheduler;

    private double finalAverageWaitingTime = 0;
    private double finalServiceWaitingTime = 0;
    private int peakHour = 0;

    private FileWriterClass fileWriterClass;

    public List<Task> generatedTasks;

    private StringBuilder output = new StringBuilder();

    public SimulationManager(int numberOfClients, int numberOfServers, int minArrival, int maxArrival, int minProcessingTime, int maxProcessingTime, int timeLimit, SelectionPolicy selectionPolicy) throws IOException {
        this.numberOfClients = numberOfClients;
        this.numberOfServers = numberOfServers;
        this.minArrivalTime = minArrival;
        this.maxArrivalTime = maxArrival;
        this.minProcessingTime = minProcessingTime;
        this.maxProcessingTime = maxProcessingTime;
        this.timeLimit = timeLimit;
        fileWriterClass = new FileWriterClass();
        scheduler = new Scheduler(numberOfServers, numberOfClients);
        scheduler.changeStrategy(selectionPolicy);
        generateNRandomTasks();
    }

    public int getTimeLimit() {
        return timeLimit;
    }

    public void setTimeLimit(int timeLimit) {
        this.timeLimit = timeLimit;
    }

    public int getMaxProcessingTime() {
        return maxProcessingTime;
    }

    public void setMaxProcessingTime(int maxProcessingTime) {
        this.maxProcessingTime = maxProcessingTime;
    }

    public int getMinProcessingTime() {
        return minProcessingTime;
    }

    public void setMinProcessingTime(int minProcessingTime) {
        this.minProcessingTime = minProcessingTime;
    }

    public int getNumberOfServers() {
        return numberOfServers;
    }

    public void setNumberOfServers(int numberOfServers) {
        this.numberOfServers = numberOfServers;
    }

    public int getNumberOfClients() {
        return numberOfClients;
    }

    public void setNumberOfClients(int numberOfClients) {
        this.numberOfClients = numberOfClients;
    }

    public int getMaxArrivalTime() {
        return maxArrivalTime;
    }

    public void setMaxArrivalTime(int maxArrivalTime) {
        this.maxArrivalTime = maxArrivalTime;
    }

    public int getMinArrivalTime() {
        return minArrivalTime;
    }

    public void setMinArrivalTime(int minArrivalTime) {
        this.minArrivalTime = minArrivalTime;
    }

    public Scheduler getScheduler() {
        return scheduler;
    }

    public List<Task> getGeneratedTasks() {
        return generatedTasks;
    }

    public void setGeneratedTasks(List<Task> generatedTasks) {
        this.generatedTasks = generatedTasks;
    }

    private void generateNRandomTasks() {
        Random rand = new Random();
        generatedTasks = new ArrayList<>();
        for (int i = 0; i < numberOfClients; i++) {
            int arrivalTime = rand.nextInt(maxArrivalTime - 1) + minArrivalTime;
            int processingTime = rand.nextInt(maxProcessingTime - 1) + minProcessingTime;
            generatedTasks.add(new Task(arrivalTime, processingTime));
        }
        for (int i = 0; i < generatedTasks.size() - 1; i++) {
            for (int j = i + 1; j < generatedTasks.size(); j++) {
                if (generatedTasks.get(i).getArrivalTime() > generatedTasks.get(j).getArrivalTime()) {
                    int temp = generatedTasks.get(i).getArrivalTime();
                    generatedTasks.get(i).setArrivalTime(generatedTasks.get(j).getArrivalTime());
                    generatedTasks.get(j).setArrivalTime(temp);
                }
            }
        }
    }

    public StringBuilder getOutput() {
        return output;
    }

    @Override
    public void run() {
        //new Runnable()
        int currentTime = 0;
        int maximumServiceTime = 0;
        boolean isFound;
        while (currentTime <= timeLimit) {
            System.out.print("Time: " + currentTime + "\nWaiting clients: ");

            output.append("Time: ").append(currentTime).append("\nWaiting clients: ");

            fileWriterClass.getPrintWriter().print("Time " + currentTime + "\nWaiting clients: ");
            isFound = false;
            for (int i = 0; i < generatedTasks.size(); i++) {
                if (generatedTasks.get(i).getArrivalTime() == currentTime) {
                    scheduler.dispatchTask(generatedTasks.get(i));
                    generatedTasks.remove(i);
                    isFound = true;
                }
                if (isFound) {
                    i--;
                    isFound = false;
                }
            }

            System.out.println(displayWaitingTasks(generatedTasks));

            output.append(displayWaitingTasks(generatedTasks)).append("\n");

            fileWriterClass.getPrintWriter().println(displayWaitingTasks(generatedTasks));

            for (Server si : scheduler.getServers()) {
                if (si.averageServiceTime.get() > maximumServiceTime) {
                    maximumServiceTime = si.averageServiceTime.get();
                    peakHour = currentTime;
                }
            }
            System.out.print(displayQueues(scheduler));

            output.append(displayQueues(scheduler));

            fileWriterClass.getPrintWriter().print(displayQueues(scheduler));
            if (generatedTasks.size() == 0 && emptyServers(scheduler))
                break;
            currentTime++;
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        computeStatistics(scheduler);
        fileWriterClass.getPrintWriter().close();
        Task.count = 0;
    }

    public boolean emptyServers(Scheduler scheduler) {
        for (Server si : scheduler.getServers())
            if (si.getTasks().size() != 0)
                return false;
        return true;
    }

    private void computeStatistics(Scheduler scheduler) {
        for (Server si : scheduler.getServers()) {
            double averageWaitingTime = 0;
            if (Double.parseDouble(String.valueOf(si.averageWaitingTime)) != 0 && Double.parseDouble(String.valueOf(si.queueTotalSize)) != 0)
                averageWaitingTime = Double.parseDouble(String.valueOf(si.averageWaitingTime)) / Double.parseDouble(String.valueOf(si.queueTotalSize));
            finalAverageWaitingTime += averageWaitingTime;
            double serviceWaitingTime = 0;
            if (Double.parseDouble(String.valueOf(si.averageServiceTime)) != 0 && Double.parseDouble(String.valueOf(si.queueTotalSize)) != 0)
                serviceWaitingTime = Double.parseDouble(String.valueOf(si.averageServiceTime)) / Double.parseDouble(String.valueOf(si.queueTotalSize));
            finalServiceWaitingTime += serviceWaitingTime;
        }
        double finalAWT = finalAverageWaitingTime / scheduler.getServers().size();
        double finalAST = finalServiceWaitingTime / scheduler.getServers().size();
        System.out.println("Average Waiting Time: " + String.format("%.2f", finalAWT) + ", Average Service time: " + String.format("%.2f", finalAST) + ", Peak Hour: " + peakHour);
        output.append("Average Waiting Time: ").append(String.format("%.2f", finalAWT)).append(", Average Service time: ").append(String.format("%.2f", finalAST)).append(", Peak Hour: ").append(peakHour);
        fileWriterClass.getPrintWriter().println("Average Waiting Time: " + String.format("%.2f", finalAWT) + ", Average Service time: " + String.format("%.2f", finalAST) + ", Peak Hour: " + peakHour);
    }

    public String displayWaitingTasks(List<Task> taskList) {
        StringBuilder toReturn = new StringBuilder();
        for (Task ti : taskList) {
            toReturn.append("(").append(ti.getID()).append(", ").append(ti.getArrivalTime()).append(", ").append(ti.getProcessingTime()).append("); ");
        }
        return toReturn.toString();
    }

    public String displayQueues(Scheduler scheduler) {
        int j = 1;
        StringBuilder toReturn = new StringBuilder();
        for (Server si : scheduler.getServers()) {
            if (si.getTasks().size() == 0) {
                toReturn.append("Queue ").append(j).append(": closed\n");
            } else {
                toReturn.append("Queue ").append(j).append(": ").append(si.getTasksFormatted()).append("\n");
            }
            j++;
        }
        return toReturn.toString();
    }
}