package Main;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Server implements Runnable {
    private BlockingQueue<Task> tasks;
    private AtomicInteger waitingPeriod;
    public AtomicInteger queueTotalSize;
    public AtomicInteger averageWaitingTime;
    public AtomicInteger averageServiceTime;

    public Server(int size) {
        tasks = new ArrayBlockingQueue<>(size);
        waitingPeriod = new AtomicInteger(0);
        averageWaitingTime = new AtomicInteger(0);
        queueTotalSize = new AtomicInteger(0);
        averageServiceTime = new AtomicInteger(0);
    }

    public BlockingQueue<Task> getTasks() {
        return tasks;
    }

    public String getTasksFormatted() {
        StringBuilder toReturn = new StringBuilder();
        for (Task ti : tasks) {
            toReturn.append("(").append(ti.getID()).append(", ").append(ti.getArrivalTime())
                    .append(", ").append(ti.getProcessingTime()).append("); ");
        }
        return toReturn.toString();
    }

    public void setTasks(BlockingQueue<Task> tasks) {
        this.tasks = tasks;
    }

    public void addTask(Task newTask) {
        queueTotalSize.getAndIncrement();
        averageServiceTime.getAndAdd(newTask.getProcessingTime());
        tasks.add(newTask);
        if (tasks.size() > 1) {
            for (Task ti: tasks) {
                averageWaitingTime.getAndAdd(ti.getProcessingTime());
            }
            averageWaitingTime.getAndAdd(-newTask.getProcessingTime());
        }
        waitingPeriod.set(waitingPeriod.get() + newTask.getProcessingTime());
    }

    @Override
    public void run() {
        while (true) {
            if (tasks.size() > 0) {
                Task getTask = tasks.element();
                getTask.setProcessingTime(getTask.getProcessingTime() - 1);
                waitingPeriod.getAndDecrement();
                if (getTask.getProcessingTime() == 0)
                    tasks.remove(getTask);
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public AtomicInteger getWaitingPeriod() {
        return waitingPeriod;
    }

    public void setWaitingPeriod(AtomicInteger waitingPeriod) {
        this.waitingPeriod = waitingPeriod;
    }
}